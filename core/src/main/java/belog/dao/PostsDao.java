package belog.dao;


import belog.dao.common.CommonDao;
import belog.pojo.po.Posts;

/**
 * @author Beldon
 */
public interface PostsDao extends CommonDao<Posts> {
}
