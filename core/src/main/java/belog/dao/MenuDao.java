package belog.dao;

import belog.dao.common.CommonDao;
import belog.pojo.po.Menu;

/**
 * Created by Beldon
 */
public interface MenuDao extends CommonDao<Menu> {
}
