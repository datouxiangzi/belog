package belog.dao.impl;


import belog.dao.PostMetaDao;
import belog.dao.common.impl.CommonDaoImpl;
import belog.pojo.po.PostMeta;
import org.springframework.stereotype.Repository;

/**
 * @author Beldon
 */
@Repository("PostMetaDao")
public class PostMetaDaoImpl extends CommonDaoImpl<PostMeta> implements PostMetaDao {
}
